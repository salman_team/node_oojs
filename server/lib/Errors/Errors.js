const Errors = {
  InternalServerError: {
    status: 500,
    message: "Internal Server Error"
  }
}

module.exports = Errors;