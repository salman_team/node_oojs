'use strict'
const Errors = require('../Errors/Errors');
const UserService = require('../Services/UserServices');
const Constants = require('../Common/Constants');
class PublicRouter {
  constructor() {

  }
  init(app) {
    console.log('Intializing Routers'); // Need to remove temp purpose
    app.get('/user', async function (req, res) {
      console.log('/users');
      try {
        let userServices = new UserService();   // for each hit it will create new instance of user service.
        let response = await userServices.getUsers(req.params);
        res.status(Constants.status).send(response);
      } catch (err) {
        res.status(Errors.InternalServerError.status).send(Errors.InternalServerError.message)
      }
    });
  }
}

module.exports = PublicRouter;