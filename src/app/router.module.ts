import { Routes, RouterModule, Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { Child1Component } from './components/home/child1/child1.component';
import { LoginComponent } from './components/public-components/login/login.component';

const Routers: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent, children: [
      // { path: '', redirectTo: '/id' ,  pathMatch: 'full'},
      { path: ':id', component: Child1Component }
    ]
  },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(Routers)],
  exports: [RouterModule]
})


export class AppRouterModule {

}